<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Keyword.php
 * Created by iwan rahardi p.
 * Created on 15052016 1413
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get keyword data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Keyword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('mainmodel');
        $this->load->model('keywordinfomodel');
    }

    public function get_list() {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $q = $this->keywordinfomodel->getList();
            if ($q) {
                foreach ($q AS $key => $value) {
                    $data[] = array(
                        'id' => $value->noid,
                        'keyword' => utf8_encode($value->keyword),
                        'description' => utf8_encode($value->description),
                        'officer' => utf8_encode($value->officer),
                        'deputy' => utf8_encode($value->deputy),
                        'status' => $value->status
                    );
                }

                $result = 200;
                $count = count($data);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }

    public function get_data($source, $storeId) {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $q = $this->storeinfomodel->getStoreInfo($source, $storeId);
            if ($q) {
                foreach ($q AS $key => $value) {
                    $tempImages = array();
                    if (!empty($value->image)) {
                        $value->image = json_decode($value->image, true);

                        foreach ($value->image AS $keyImages => $valueImages) {
                            $tempImages[] = $this->config->item('data_url') . '/' . $source . '/stores/' . $valueImages;
                        }
                    }

                    $value->image = $tempImages;

                    if (!empty($value->contact)) {
                        $value->contact = json_decode($value->contact);
                        if (isset($value->contact->phone_text)) {
                            $tempText = substr($value->contact->phone_text, 0, 1);
                            if ('0' === $tempText) {
                                $value->contact->phone_text = '+62' . substr($value->contact->phone_text, 1);
                            }
                            $value->contact->phone_text = urlencode($value->contact->phone_text);
                        }
                    }

                    $data[] = $value;
                }

                $result = 200;
                $count = count($data);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }

    public function save_keyword() {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            if ($this->input->server('REQUEST_METHOD') === 'POST') {
                $postData = file_get_contents('php://input');
                $jsonData = json_decode($postData, true);

                $data = array(
                    'keyword' => $jsonData['keyword']
                    , 'description' => $jsonData['description']
                    , 'officer' => $jsonData['officer']
                    , 'deputy' => $jsonData['deputy']
                    , 'status' => $jsonData['status']
                );

                $insertId = $this->keywordinfomodel->saveKeyword($jsonData['id'], $data);
                if (0 < $insertId) {
                    $result = 200;
                    $count = 1;
                    $message = 'SUCCESS';

                    $data['id'] = $insertId;
                }
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }
}