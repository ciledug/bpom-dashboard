<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once 'include_header.php'; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/jquery-confirm/jquery-confirm.min.css">
<style type="text/css">
#dialog-content-image {
    width: 100%;
    height: auto;
}
</style>

<div class="container">
    <div class="row first-row">
        <div class="col-md-12 col-sm-12">
            <h3><?php echo $data['title']; ?></h3>
            <hr />
        </div>
    </div>

    <div class="row">
        <div id="source-btn-container" class="col-md-12 col-sm-12">
            <button type="button" class="btn btn-sm btn-default btn-source shadow-header" id="btn-add-keyword" value="Tambah">
                <span class="glyphicon glyphicon-plus"></span> Tambah
            </button>
        </div>
    </div>

    <div class="row"><div class="col-sm-12 col-md-12">&nbsp;</div></div>

    <div class="row">
        <div class="col-md-12 col-sm-12" id="data-list-container">
            <table id="data-table" class="table table-striped table-bordered hover" style="width:100%;">
                <thead>
                    <tr>
                        <th>Obj</th>
                        <th>No.</th>
                        <th>Keyword</th>
                        <th>Deskripsi</th>
                        <th>Petugas</th>
                        <th>Deputi</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="data-table-body"></tbody>
            </table>
        </div>
    </div>
</div>

<div id="dialog-add-keyword">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Keyword</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" id="dialog-add-keyword-keyword" class="form-control" name="dialog-add-keyword-keyword" placeholder="Kata kunci pencarian" style="width:100%;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Deskripsi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div class="col-sm-8 col-md-8">
                        <textarea id="dialog-add-keyword-description" class="form-control" name="dialog-add-keyword-description" rows="5" style="resize:vertical;"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Petugas</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div class="col-sm-8 col-md-8">
                        <input type="text" id="dialog-add-keyword-officer" class="form-control" name="dialog-add-keyword-officer" placeholder="Nama petugas" style="width:100%;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Deputi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div class="col-sm-8 col-md-8">
                        <select id="dialog-add-keyword-deputy" class="form-control" name="dialog-add-keyword-deputy">
                            <option value="0">Pilih Deputi ---</option>
                            <option value="1">Deputi 1</option>
                            <option value="2">Deputi 2</option>
                            <option value="3">Deputi 3</option>
                            <option value="4">Deputi 4</option>
                        </select>
                    </div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Status</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div class="col-sm-8 col-md-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="dialog-add-keyword-status" name="dialog-add-keyword-status" checked="checked" />
                                Aktif
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="add-keyword-id" name="add-keyword-id" value="0" />
    <input type="hidden" id="add-keyword-status" name="add-keyword-status" value="0" />
    <input type="hidden" id="add-keyword-datatable-row-idx" name="add-keyword-datatable-row-idx" value="0" />
</div>

<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('js_url'); ?>/common-vars.js"></script>
<script type="text/javascript">
var dataTable = $('#data-table').DataTable({
    ordering: false,
    paging: true,
    info: false,
    searching: true,
    pageLength: 15,
    scrollY: 600,
    columns: [
        { 'width': '1%' }       // obj
        , { 'width': '5%' }     // no
        , { 'width': '20%' }    // keyword
        , { 'width': '35%' }    // deskripsi
        , { 'width': '20%' }    // petugas
        , { 'width': '5%' }     // deputi
        , { 'width': '15%' }    // status
    ],
    columnDefs: [
        { 'targets': [0], 'visible': false }
    ],
    createdRow: function(row, data, index) {
    }
});

var addKeywordDialog = $.confirm({
    content: $('#dialog-add-keyword').html()
    , title: 'Penambahan Kata Pencarian'
    , boxWidth: '50%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        ok: {
            text: 'Simpan',
            btnClass: 'btn-primary',
            action: function() {
                saveKeyword();
                return false;
            }
        },
        close: {
            text: 'Batal'
        }
    }
});

$('#data-table tbody').on('click', 'tr', function(e) {
    openAddKeywordDialogForEdit(dataTable.row(this));
}).on('click', '.btn-violation', function(e) {
    return false;
});

$('#btn-add-keyword').click(function(e) {
    e.preventDefault();
    openAddKeywordDialog();
});

saveKeyword = function(keywordId, status, keyword, description) {
    var data = {
        'id': addKeywordDialog.$content.find('#add-keyword-id').val()
        , 'keyword': addKeywordDialog.$content.find('#dialog-add-keyword-keyword').val()
        , 'description': addKeywordDialog.$content.find('#dialog-add-keyword-description').val()
        , 'officer': addKeywordDialog.$content.find('#dialog-add-keyword-officer').val()
        , 'deputy': addKeywordDialog.$content.find('#dialog-add-keyword-deputy').val()
        , 'status': addKeywordDialog.$content.find('#dialog-add-keyword-status')
    };

    var rowIdx = parseInt(addKeywordDialog.$content.find('#add-keyword-datatable-row-idx').val(), 10);
    // console.log('rowIdx: ' + rowIdx);

    if ('' === data.keyword) {
        alert('Keyword/Kata Kunci tidak boleh kosong');
        return false;
    }

    if ($(data.status).is(':checked')) {
        data.status = 1;
    } else {
        data.status = 0;
    }

    $.loadingBlockShow();

    var url = API_URL + '/data/Keyword/save_keyword';
    $.ajax({
        url : url,
        type : 'POST',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        dataType : 'json'
    })
    .done(function(json) {
        // console.log(JSON.stringify(json));
        if ((200 === json.result) && (1 === json.count)) {
            alert('Keyword/Kata Kunci berhasil disimpan');
            addKeywordDialog.close();

            var editLink = '<a style="cursor:pointer;">' + json.data.keyword + '</a>';
            var rowNumber = dataTable.rows().count() + 1;
            if (0 < rowIdx) {
                rowNumber = rowIdx + 1;
            }

            var arrayData = [
                json.data
                , rowNumber
                , editLink
                , json.data.description
                , json.data.officer
                , json.data.deputy
                // , '<a href="#" class="data-table-link product-info-link" data-source="' + source + '" data-selected="product" data-id="' + b.product_id + '">' + b.title + '</a>'
                , (1 === parseInt(json.data.status, 10)) ? 'Aktif' : 'Tidak aktif'
            ];

            if (0 === rowIdx) {
                dataTable.row.add(arrayData).draw();
            } else {
                dataTable.row(rowIdx).data(arrayData).invalidate();
            }
        }

        $.loadingBlockHide();
    });
};

openAddKeywordDialog = function() {
    addKeywordDialog.open();
    addKeywordDialog.$content.find('#add-keyword-id').val('0');
    addKeywordDialog.$content.find('#dialog-add-keyword-keyword').val('');
    addKeywordDialog.$content.find('#dialog-add-keyword-description').val('');
    addKeywordDialog.$content.find('#dialog-add-keyword-officer').val('');
    addKeywordDialog.$content.find('#dialog-add-keyword-deputy').val('0');
    addKeywordDialog.$content.find('#dialog-add-keyword-status').val('0');
    addKeywordDialog.$content.find('#add-keyword-datatable-row-idx').val('0');
};

openAddKeywordDialogForEdit = function(dataTableRow) {
    var objKeyword = dataTableRow.data();
    // console.log(objKeyword);
    // console.log(dataTableRow.index());
    addKeywordDialog.open();
    addKeywordDialog.$content.find('#add-keyword-id').val(objKeyword[0].id);
    addKeywordDialog.$content.find('#dialog-add-keyword-keyword').val(objKeyword[0].keyword);
    addKeywordDialog.$content.find('#dialog-add-keyword-description').val(objKeyword[0].description);
    addKeywordDialog.$content.find('#dialog-add-keyword-officer').val(objKeyword[0].officer);
    addKeywordDialog.$content.find('#dialog-add-keyword-deputy').val(objKeyword[0].deputy);
    addKeywordDialog.$content.find('#add-keyword-datatable-row-idx').val(dataTableRow.index());

    if (1 !== parseInt(objKeyword[0].status)) {
        addKeywordDialog.$content.find('#dialog-add-keyword-status').prop('checked', false);
    }
    addKeywordDialog.$content.find('#dialog-add-keyword-status').val(objKeyword[0].status);
};

getKeywordList = function() {
    $.loadingBlockShow();

    dataTable.rows().remove().draw(false);
    $.get(API_URL + '/data/Keyword/get_list', function(response) {
        // console.log(JSON.stringify(response));
        if ((200 === response.result) && (0 < response.count)) {
            populateKeywordTable(response.data);
        }

        $.loadingBlockHide();
    });
};

populateKeywordTable = function(responseData) {
    var seq = 1;
    $.each(responseData, function(a, b) {
        var editLink = '<a style="cursor:pointer;">' + b.keyword + '</a>';
        var newRow = dataTable.row.add([
            b
            , seq + '.'
            , editLink
            , b.description
            , b.officer
            , b.deputy
            // , '<a href="#" class="data-table-link product-info-link" data-source="' + source + '" data-selected="product" data-id="' + b.product_id + '">' + b.title + '</a>'
            , (1 === b.status) ? 'Aktif' : 'Tidak aktif'
        ]).draw().node();

        seq++;
    });
};

$(document).ready(function() {
    $('#dialog-add-keyword').hide();
    getKeywordList();
});

</script>