<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * General.php
 * Created by iwan rahardi p.
 * Created on 01042018 0526
 * Built on Lubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get general info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class General extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('mainmodel');
        $this->load->model('generalinfomodel');
    }

    public function index() {
    }

    public function get_keywords() {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $tempKeywordList = array();
            $tempCountSource = 0;
            $tempCountProduct = 0;
            $tempCountStore = 0;


            $q = $this->generalinfomodel->getKeywords();
            if ($q) {
                foreach ($q AS $key => $value) {
                    ;
                }
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ), true);
    }
}