<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Owner.php
 * Created by iwan rahardi p.
 * Created on 28022016 0242
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get user data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Owner extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('mainmodel');
        $this->load->model('ownerinfomodel');
    }

    public function get_data($source, $ownerId) {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $q = $this->ownerinfomodel->getOwnerInfo($source, $ownerId);
            if ($q) {
                foreach ($q AS $kqy => $value) {
                    if (!empty($value->contact)) {
                        $value->contact = json_decode($value->contact);
                        if (isset($value->contact->phone_text)) {
                            $tempText = substr($value->contact->phone_text, 0, 1);
                            if ('0' === $tempText) {
                                $value->contact->phone_text = '+62' . substr($value->contact->phone_text, 1);
                            }
                            $value->contact->phone_text = urlencode($value->contact->phone_text);
                        }
                    }

                    $tempImages = array();
                    if (!empty($value->image)) {
                        $value->image = json_decode($value->image, true);
                        foreach ($value->image AS $key => $val) {
                            $tempImages[] = $this->config->item('data_url') . '/' . $source . '/owners/' . $val;
                        }
                        $value->image = $tempImages;
                    }

                    $data[] = $value;
                }

                $result = 200;
                $count = count($data, true);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }
}