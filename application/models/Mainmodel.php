<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mainmodel extends CI_Model {

    function __construct() {
		parent::__construct();
    }
    
	public function cek_session(){
		// $this->load->library('session');
		return ($this->session->userdata('user_id') == TRUE ? TRUE : FALSE);
	}
	
	public function welcome(){
		// @session_start();
		$username = $this->session->userdata('user_name');
		$lastvisitdate = $this->session->userdata('last_login');
		$message  = 'Welcome <b>'.$username.'</b>, You are login at '.$this->convdate($lastvisitdate,true);
		//$message .= ' | <a href="'.base_url().'profile/change_password">Change Password</a>';
		$message .= ' | <a href="' . site_url('auth/logout') . '">Logout</a>';
		return $message;
	}

	function showfooter() {
		$this->load->helper('html');
		$footer = '<div style="text-align:left;float:left;width:47%;" class="footer">&copy; Copyright '.date("Y").' CreativeHead Technology. All Right Reserved </div><div style="text-align:right;float:left;width:49%;" class="footer"><font style="color:#000;">Powered By : </font><img src="'.$this->config->item("image_url").'indexpoliti.png" style="width:150px;height:25px;margin-top:-7px;" alt="indexpolitica.com" align="middle" border="0" /> <img src="'.$this->config->item("image_url").'creativehead.png" style="width:109px;height:45px;margin-top:-5px;" alt="creativehead.net" align="middle" border="0" /></div>';
		return $footer;
	}

	public function showmenu(){
		$id = $this->session->userdata('user_id');
		$menu = '';
		$privileges = explode('|', $this->get_privileges($id));
		
		$mymodul = $this->get_modul(0);
		foreach ($mymodul as $row) {
			if(in_array($row['modul_id'], $privileges)) {
			    // $applabel = $row['modul_name'];
			    $mysub = $this->get_modul($row['modul_id']);
			    if (count($mysub) > 0) {
			        $menu .= $this->createDropdownMenu($privileges, $row['modul_name'], $mysub);
			    } else {
			        $menu .= '<li><a href="' . site_url($row['modul_link']) . '">' . $row['modul_name'] . '</a></li>';
			    }
			}
		}

		return $menu;
	}

	private function get_privileges($user_id) {
		$this->db->select('*')->from('user_account')->where('user_id',$user_id);
		$q = $this->db->get();
		$r = $q->row_array();
		return $r['privileges'];
	}

	public function get_modul($root=0) {
		$this->db->select('modul_id, modul_name, modul_link');
		$this->db->from('menu_module');
		$this->db->where('modul_root_id',$root)->where('active','1');
		$this->db->order_by('pos','asc');
		$this->db->order_by('modul_id','asc');
		$q = $this->db->get();
		return $q->result_array();
	}
	
	public function get_all_modul() {
		$this->db->select('modul_id,modul_root_id,modul_name, modul_link');
		$this->db->from('menu_module');
		$this->db->where('modul_id != ','1')->where('modul_id != ','5');
		$this->db->order_by('modul_id','asc');
		$q = $this->db->get();
		return $q->result_array();
	}
        
	private function createDropdownMenu($privileges, $applabel, $theMenu) {
	    $menu = '<li class="dropdown">';
	    $menu .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $applabel . ' <span class="caret"></span></a>';
	    $menu .= '<ul class="dropdown-menu" role="menu">';
	    foreach ($theMenu as $srow) {
            if(in_array($srow['modul_id'], $privileges))
            {
                $menu .= '<li>';
                $menu .= '<a href="' . base_url($srow['modul_link']) . '">' . $srow['modul_name'] . '</a>';
                $menu .= '</li>';
            }
	    }
	    $menu .= '</ul>';
	    $menu .= '</li>';
	    
	    return $menu;
	}

	function convdate($valdate,$time=false){
		if(strlen($valdate>10)){
			list($dateval,$timeval) = explode(' ',$valdate);
			list($yy,$mm,$dd) = explode('-',$dateval);
			$ndate = $dd.'-'.$mm.'-'.$yy;
			if($time)
				$ndate = $ndate.' '.$timeval;
		} else {
			list($yy,$mm,$dd) = explode('-',$valdate);
			$ndate = $dd.'-'.$mm.'-'.$yy;
		}
		return $ndate;
	}

	function convdate2($valdate){
		list($yy,$mm,$dd) = explode('-',$valdate);
		$ndate = $dd.'-'.$mm.'-'.$yy;
		return $ndate;
	}
	
	function cleanup($str){
		$messy = array("\n","\r","\r\n","'","\\");
		return str_replace($messy,'',$str);
	}
}
