<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Statistic.php
 * Created by iwan rahardi p.
 * Created on 22072018 1900
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : starts things ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Statistic extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->library('utils');
        $this->load->model('mainmodel');
        $this->load->model('statisticinfomodel');
    }

    public function get_summary($source) {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $data = $this->statisticinfomodel->getSummary($source);

            if (0 < count($data)) {
                $this->load->model('crawlerschedulesinfomodel');

                foreach ($data AS $key => $value) {
                    $value['last'] = $this->crawlerschedulesinfomodel->getLastRun($source, $value['keyword']);
                    $value['color'] = $this->utils->createRandomColor();
                    $data[$key] = $value;
                }
            }

            $result = 200;
            $message = 'SUCCESS';
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => count($data),
            'message' => $message,
            'data' => $data
        ));
    }
}