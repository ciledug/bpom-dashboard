<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Keywordinfomodel.php
 * Created by iwan rahardi p.
 * Created on 15052018 0036
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get keyword info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Keywordinfomodel extends CI_Model {

     public function __construct() {
        parent::__construct();
     }

     public function getList() {
        $data = false;
        $this->db->select('*');
        $this->db->from('keywords');
        $this->db->order_by('noid', 'ASC');

        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
     }

     public function getKeywordInfo($source, $storeId) {
        $data = false;
        $this->db->select('source_website, store_id, store_name, owner_fullname, full_url, description, image');
        $this->db->select('address, latitude, longitude, contact');
        $this->db->from('stores_' . $source);
        $this->db->where('store_id', $storeId);

        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
     }

     public function saveKeyword($noid, $data) {
        $insertId = 0;

        if (0 == $noid) {
            if ($this->db->insert('keywords', $data)) {
                $insertId = $this->db->insert_id();
            }
        } else {
            $this->db->where('noid', $noid);
            if ($this->db->update('keywords', $data)) {
                $insertId = $noid;
            }
        }

        return $insertId;
     }
}