<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php echo $this->config->item('web_title'); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/bootstrap-3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/datatables/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/datatables/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('css_url')?>/login.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('css_url')?>/bpom.css">

<script type="text/javascript" src="<?php echo $this->config->item('js_url'); ?>/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/small-loading-modal/assets/js/jquery.loading.block.js"></script>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="<?php echo $this->config->item('image_url')?>/favicon.ico" />
<link rel="preload" href="<?php echo $this->config->item('assets_url'); ?>/small-loading-modal/assets/img/default.svg" as="image" />
<link rel="preload" href="<?php echo $this->config->item('assets_url'); ?>/images/no_image_available.png" as="image" />
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top shadow-header" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php echo $this->mainmodel->showmenu(); ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->userdata('user_email'); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <!--
                        <li><a href="<?php echo base_url('/profile'); ?>">Profile</a></li>
                        <li class="divider"></li>
                        -->
                        <li><a href="<?php echo base_url('/auth/logout'); ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
