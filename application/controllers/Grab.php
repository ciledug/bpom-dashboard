<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Grab.php
 * Created by iwan rahardi p.
 * Created on 01042018 0501
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : starts things ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

 class Grab extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('mainmodel');
    }

    function index() {
        if ($this->mainmodel->cek_session()) {
            $data['data'] = array(
                'title' => 'Grab',
                'page' => 'grab'
            );
            $this->load->view($data['data']['page'], $data);
        }
    }
}