<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('accountmodel');
        $this->load->model('usermodel');
    }

    function index()
    {
        $this->checkLogin();
    }

    public function checkLogin() {
        // -- if login after browser closed ...
        if ($this->session->userdata('user_group')) {
            if ($this->session->userdata('user_group') == '99') redirect('adm/adm');
            else redirect('home');
        } else {
            redirect('login');
        }
    }

    function login() {
        $pageData['title'] = 'Login';
        $pageData['message'] = 'Anda tidak memiliki akses';
        $auth = false;

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
        	$postData = file_get_contents('php://input');
        	$jsonData = json_decode($postData, true);
        	
        	switch (strtolower($jsonData['uAct'])) {
        	    case 'login':
        	        $auth = $this->isUser($jsonData['uEmail'], $jsonData['uPass']);
        	        break;
        	        
        	    case 'register':
        	        $auth = $this->register($jsonData['uEmail'], $jsonData['uPass']);
        	        break;
        	        
        	    default:
        	        break;
        	}
                
            if ($auth) {
            	$this->genSession($auth);
//             	$this->updateSession();
            	$this->session->unset_userdata(array('flash_message' => ''));
            	$pageData['message'] = '';
            }
            
            $this->sendjson->send($pageData);

        } else {
            $this->load->view('login');
        }
    }

    private function isUser($email, $pass)
    {
    	$data = $this->usermodel->getUserLogin($email);
    	if ($data) {
    		$this->load->library('utils');
    		if (strcmp($this->utils->decryptPassword($data['user_pass']), $pass) != 0 ) {
    			$data = false;
    		}
    	}
        return $data;
    }
    
    private function register($email, $pass)
    {
        $data = $this->usermodel->getUserLogin($email);
        if (!$data) :
            $this->load->library('utils');
            $pass = $this->utils->createPassword($pass);
            $data = array('user_email' => $email, 'user_pass' => $pass);
            
            $uid = $this->accountmodel->add($data);
            if ($uid > 0) :
                $data = $this->usermodel->getUserLogin($email);
            else :
                $data = false;
            endif;
        else :
            $data = false;
        endif;
        
        return $data;
    }

    private function genSession($data)
    {
        $data['last_login'] = date("Y-m-d H:i:s");
        $this->session->set_userdata($data);
    }

    private function updateSession()
    {
        $data = array('last_login'=> date("Y-m-d H:i:s"));
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('user_account',$data);
        return true;
    }

    public function logout()
    {
        $this->session->unset_userdata('user_group');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata($this->session->all_userdata());
        $this->session->sess_destroy();
        $this->checkLogin();
    }
}

/* End of file */
