<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $this->config->item('web_title'); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/bootstrap-3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('css_url')?>/bpom.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('css_url')?>/login.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2•/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="<?php echo $this->config->item('image_url')?>/favicon.ico" />
</head>

<body>
<div class="vertical-center">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="row">
                    <div class="panel panel-default login-panel">
                        <div class="panel-heading"><?php echo $this->config->item('web_title'); ?></div>
                        <div class="panel-body">
                            <form name="loginForm" id="loginForm">
                                <div>
                                    <div class="form-group">
                                        <input name="uEmail" id="uEmail" class="form-control" type="text" placeholder="Email" value="demo@localhost" required>
                                    </div>
                                    <div class="form-group">
                                        <input name="uPass" id="uPass" class="form-control" type="password" placeholder="Password" value="123456" required>
                                    </div>
                                    <!-- 
                                    <div class="form-group">
                                        <div class="g-recaptcha" data-sitekey="6LfHl_4SAAAAABmIVefO3oIw8KVoaDuoK9d-mkqp" style="text-align: center"></div>
                                    </div>
                                     -->
                                </div>
                            </form>
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="button" id="btn-login" class="btn btn-primary" value="login">Login</button>
                                    <button type="button" id="btn-register" class="btn btn-info" value="register">Register</button>
                                </div>
                                <!--
                                <div class="col-md-6 text-right">
                                    <div>
                                        <button type="button" id="btn-forgot-pass" class="btn btn-primary" value="forgot">Forgot Password</button>
                                    </div>
                                </div>
                                //-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="new_account_dialog">
	<div></div>
	<div></div>
</div>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-56088174-3', 'auto');
ga('send', 'pageview');
</script>

<script type="text/javascript" src="<?php echo $this->config->item('js_url')?>/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url')?>/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/small-loading-modal/assets/js/jquery.loading.block.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('js_url')?>/common-vars.js"></script>
<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
<script type="text/javascript">
var email = '';
var pass = '';
var action = '';
$('.btn').click(function(e) {
	e.preventDefault();
	email = $('#uEmail').val();
	pass = $('#uPass').val();
	action = $(this).val().trim();

    if (email.trim() === '') {
        alert('Email may not be empty');
        return false;
    }

    if (pass.trim() === '') {
        alert('Password may not be empty');
        return false;
    }

    if ('login' === action || 'register' === action) {
        doLogin();
    } else if ('forgot' === action) {
        doForgot();
    }
});

doLogin = function() {
    $.loadingBlockShow();
	$.ajax({
		// url : API_URL + '/auth/login',
        url : './auth/login',
		type : 'post',
        headers: {'Access-Control-Allow-Origin':'http://127.0.0.1'},
		contentType : 'application/json; charset=utf-8',
		data : JSON.stringify({ uEmail:email, uPass:pass, uAct:action }),
		dataType : 'json'
	})
	.done(function(json) {
        // console.log(JSON.stringify(json));
		if (json.message.trim() === '') {
			// location.href = API_URL + '/home';
            location.href = './home';
		} else {
			alert(json.message);
		}
        $.loadingBlockHide();
	});
};
//doForgot = function() {
//	$.ajax({
//		url : BASE_URL + '/auth/reset',
//		type : 'post',
//		contentType : 'application/json; charset=utf-8',
//		data : JSON.stringify({ uEmail:email, uAct:action}),
//		dataType : 'json'
//	})
//	.done(function(json) {
//		
//	});
//};

$(document).ready(function() {
});
</script>
</body>
</html>