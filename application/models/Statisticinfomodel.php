<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Statisticinfomodel.php
 * Created by iwan rahardi p.
 * Created on 22072018 1907
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get statistic data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Statisticinfomodel extends CI_Model {

     public function __construct() {
        parent::__construct();
     }

     public function getSummary($source) {
        $data = array();

        $command = 'SELECT k.noid, k.keyword ';
        $command .= 'FROM keywords k ';
        $command .= 'ORDER BY k.noid ASC ';
        $queryKeyword = $this->db->query($command);

        if (0 < $queryKeyword->num_rows()) {
            $resultKeyword = $queryKeyword->result_array();
            foreach ($resultKeyword AS $keyKeyword => $valueKeyword) {
                $command = 'SELECT COUNT(p.noid) AS count_rows ';
                $command .= 'FROM products_' . $source . ' p ';
                $command .= "WHERE p.keyword='" . $valueKeyword['keyword'] . "' ";

                $queryCount = $this->db->query($command);
                $resultCount = $queryCount->row();

                $data[] = array(
                    'keyword' => $valueKeyword['keyword'],
                    'count' => $resultCount->count_rows
                );
            }
        }

        return $data;
     }
 }