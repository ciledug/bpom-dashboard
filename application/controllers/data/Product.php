<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Product.php
 * Created by iwan rahardi p.
 * Created on 26022016 0833
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get grabbed data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('mainmodel');
        $this->load->model('productinfomodel');
    }

    public function index() {
    }

    public function get_list($source) {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $this->load->model('violationinfomodel');

            $q = $this->productinfomodel->getData($source);

            if ($q) {
                $images = array();

                foreach ($q AS $key => $value) {
                    if (!empty($value->product_type)) {
                        $imageName = str_replace(' ', '-', strtolower($value->product_type));
                        $value->product_type = array(
                            'title' => $value->product_type,
                            'image' => $this->config->item('image_url') . '/' . $imageName . '.png'
                        );
                    } else {
                        $value->product_type = '';
                    }

                    $images = json_decode($value->image, true);
                    if (is_array($images)) {
                        $value->image = $this->config->item('data_url') . '/' . $source . '/products/' . $images[0];
                    } else {
                        $value->image = $this->config->item('image_no_image');
                    }

                    if (empty($value->owner_fullname)) {
                        $value->owner_fullname = $value->owner_id;
                    }

                    if (empty($value->store_name)) {
                        $value->store_name = $value->store_id;
                    }

                    if (!empty($value->violation_type)) {
                        $decodedType = json_decode($value->violation_type, true);
                        $tempViolationArray = array();

                        foreach ($decodedType AS $keyViolation => $valueViolation) {
                            $qv = $this->violationinfomodel->getViolationInfo($valueViolation);
                            if ($qv) {
                                $qv[0]->label = $qv[0]->violation_type . ':' . $qv[0]->keywords;
                                unset($qv[0]->noid, $qv[0]->violation_type, $qv[0]->keywords);
                                $tempViolationArray[] = $qv[0];
                            }
                        }

                        $value->violation_type = $tempViolationArray;
                    }

                    $data[] = $value;
                }

                $result = 200;
                $count = count($data);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }

    public function get_data($source, $productId) {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $q = $this->productinfomodel->getProductInfo($source, $productId);
            if ($q) {
                $this->load->model('violationinfomodel');

                $images = json_decode($q[0]->image, true);
                if (is_array($images)) {
                    $q[0]->image = $images[0];
                    $q[0]->image = $this->config->item('data_url'). '/' . $source . '/products/' . $q[0]->image;
                } else {
                    $q[0]->image = $this->config->item('image_no_image');
                }
                

                if (!empty($q[0]->violation_type)) {
                    $decodedType = json_decode($q[0]->violation_type, true);
                    $tempViolationArray = array();

                    foreach ($decodedType AS $keyViolation => $valueViolation) {
                        $qv = $this->violationinfomodel->getViolationInfo($valueViolation);
                        if ($qv) {
                            $qv[0]->label = $qv[0]->violation_type . ':' . $qv[0]->keywords;
                            unset($qv[0]->noid, $qv[0]->violation_type, $qv[0]->keywords);
                            $tempViolationArray[] = $qv[0];
                        }
                    }

                    $q[0]->violation_type = $tempViolationArray;
                }

                $data = $q;
                $result = 200;
                $count = count($data);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }

    public function save_violation_info() {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            if ($this->input->server('REQUEST_METHOD') === 'POST') {
                $postData = file_get_contents('php://input');
                $jsonData = json_decode($postData, true);

                $data = array(
                    'is_violating' => $jsonData['violation'],
                    'information' => $jsonData['info'],
                    'officer' => $jsonData['officer'],
                    'deputy' => $jsonData['deputy']
                );

                if ($this->productinfomodel->saveViolationInfo($jsonData['source'], $jsonData['product'], $data)) {
                    $result = 200;
                    $count = 1;
                    $message = 'SUCCESS';
                }
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }
 }