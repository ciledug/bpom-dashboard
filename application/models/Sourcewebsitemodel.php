<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SourceWebsiteModel.php
 * Created by iwan rahardi p.
 * Created on 07032018 0442
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : for general info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Sourcewebsitemodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        $data = false;
        $this->db->select('name, value');
        $this->db->from('source_website');
        $this->db->where('active', '1');
        $this->db->order_by('name', 'asc');
        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
    }
}