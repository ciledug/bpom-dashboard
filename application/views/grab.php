<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once 'include_header.php'; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets_url')?>/jquery-confirm/jquery-confirm.min.css">
<style type="text/css">
#dialog-content-image {
    width: 100%;
    height: auto;
}
</style>

<div class="container">
    <div class="row first-row">
        <div class="col-md-12 col-sm-12">
            <h3><?php echo $data['title']; ?></h3>
            <hr />
        </div>
    </div>

    <div class="row">
        <div id="source-btn-container" class="col-md-12 col-sm-12">
        </div>
    </div>

    <div class="row"><div class="col-sm-12 col-md-12">&nbsp;</div></div>

    <div class="row">
        <div class="col-md-12 col-sm-12" id="data-list-container">
            <table id="data-table" class="table table-striped table-bordered hover" style="width:100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal</th>
                        <th>Keyword</th>
                        <th>Deputi</th>
                        <th>Produk</th>
                        <th>Jenis</th>
                        <th>Dugaan Pelanggaran</th>
                        <th>Melanggar</th>
                        <th>Gambar</th>
                        <th>Toko</th>
                        <th>Pemilik</th>
                    </tr>
                </thead>
                <tbody id="data-table-body"></tbody>
            </table>
        </div>
    </div>
</div>

<div id="dialog-user-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Nama</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-fullname" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Username</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-username" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Lokasi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-location" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Kontak</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-contact" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Sumber</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-source" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Deskripsi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-user-description" class="col-sm-8 col-md-8"></div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <img src="" id="dialog-content-user-image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialog-store-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Nama</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-name" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Pemilik</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-owner" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Lokasi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-address" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Kontak</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-contact" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Email</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-email" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">Sumber</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-source" class="col-sm-8 col-md-8"></div>
                </div>
                <div class="row row-odd">
                    <div class="col-sm-3 col-md-3">Deskripsi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-store-description" class="col-sm-8 col-md-8"></div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <img src="" id="dialog-content-store-image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialog-product-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row row-dialog row-odd">
                    <div class="col-sm-4 col-md-4">Nama/Judul</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-name" class="col-sm-7 col-md-7"></div>
                </div>
                <div class="row row-dialog">
                    <div class="col-sm-4 col-md-4">Toko</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-store" class="col-sm-7 col-md-7"></div>
                </div>
                <div class="row row-dialog row-odd">
                    <div class="col-sm-4 col-md-4">Pemilik</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-owner" class="col-sm-7 col-md-7"></div>
                </div>
                <div class="row row-dialog">
                    <div class="col-sm-4 col-md-4">Sumber</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-source" class="col-sm-7 col-md-7"></div>
                </div>
                <div class="row row-dialog row-odd">
                    <div class="col-sm-4 col-md-4">Dugaan Pelanggaran</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-violation" class="col-sm-7 col-md-7"></div>
                </div>
                <div class="row row-dialog">
                    <div class="col-sm-4 col-md-4">Deskripsi</div>
                    <div class="col-sm-1 col-md-1">:</div>
                    <div id="dialog-content-product-description" class="col-sm-7 col-md-7"></div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="" alt="" title="" target="_blank" id="dialog-content-product-image-link">
                            <img src="" id="dialog-content-product-image">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="dialog-content-product-source" value="" />
    <input type="hidden" id="dialog-content-product-id" value="0" />
    <input type="hidden" id="dialog-content-product-object" value="" />
</div>

<div id="dialog-violation-info">
    <div class="container-fluid">
        <div class="row">
            <div id="dialog-content-violation-description" class="col-sm-12 col-md-12"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 clearfix">&nbsp;</div>
        </div>
        <div class="row">
            <div id="dialog-content-violation-source" class="col-sm-12 col-md-12"></div>
        </div>
    </div>
</div>

<div id="dialog-more-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-3">Pelanggaran</div>
            <div class="col-sm-9 col-md-9">
                <label class="radio-inline">
                    <input type="radio" id="dialog-content-more-info-violated-no" class="dialog-content-more-info-violated" name="dialog-content-more-info-violated" value="0" /> Tidak
                </label>
                <label class="radio-inline">
                    <input type="radio" id="dialog-content-more-info-violated-yes" class="dialog-content-more-info-violated" name="dialog-content-more-info-violated" value="1" /> Ya
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3">Informasi Tambahan</div>
            <div class="col-sm-9 col-md-9 clearfix"><textarea id="dialog-more-info-information" rows="5" style="width:100%; resize:vertical;"></textarea></div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3">Petugas</div>
            <div class="col-sm-9 col-md-9">
                <input type="text" id="dialog-more-info-officer" class="form-control" placeholder="Nama petugas" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3">Deputi</div>
            <div class="col-sm-3 col-md-9">
                <select id="dialog-content-more-info-deputy" class="form-control">
                    <option value="0">Pilih Deputi ---</option>
                    <option value="1">Deputi 1</option>
                    <option value="2">Deputi 2</option>
                    <option value="3">Deputi 3</option>
                    <option value="4">Deputi 4</option>
                </select>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('js_url'); ?>/common-vars.js"></script>
<script type="text/javascript">
var dataTable = $('#data-table').DataTable({
    ordering: false,
    paging: true,
    info: false,
    searching: true,
    pageLength: 15,
    scrollY: 600,
    columns: [
        { 'width': '5%' }       // no
        , { 'width': '12%' }    // tanggal
        , { 'width': '5%' }     // keyword
        , { 'width': '3%' }     // deputi
        , { 'width': '25%' }    // produk
        , { 'width': '5%' }     // jenis
        , { 'width': '15%' }    // dugaan pelanggaran
        , { 'width': '5%' }     // melanggar
        , { 'width': '10%' }    // gambar
        , { 'width': '10%' }    // toko
        , { 'width': '15%' }    // pemilik
    ],
    columnDefs: [
        { targets: [0, 3], className: 'dt-body-right' }
        , { targets: [8], className: 'dt-body-center' }
    ],
    createdRow: function(row, data, index) {
    }
});

var userInfoDialog = $.confirm({
    content: $('#dialog-user-info').html()
    , title: 'Informasi Pemilik'
    , boxWidth: '80%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        close: {
            text: 'Tutup'
        }
    }
});

var storeInfoDialog = $.confirm({
    content: $('#dialog-store-info').html()
    , title: 'Informasi Toko'
    , boxWidth: '80%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        close: {
            text: 'Tutup'
        }
    }
});

var productInfoDialog = $.confirm({
    content: $('#dialog-product-info').html()
    , title: 'Informasi Produk'
    , boxWidth: '80%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        dialogInfo: {
            text: 'Kepastian Pelanggaran',
            btnClass: 'btn-primary',
            action: function () {
                openMoreInfoDialog();
                return false;
            }
        },
        close: {
            text: 'Tutup'
        }
    }
});

var violationInfoDialog = $.confirm({
    content: $('#dialog-violation-info').html()
    , title: 'Informasi Dugaan Pelanggaran'
    , boxWidth: '60%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        close: {
            text: 'Tutup'
        }
    }
});

var moreInfoDialog = $.confirm({
    content: $('#dialog-more-info').html()
    , title: 'Kepastian Pelanggaran'
    , boxWidth: '60%'
    , lazyOpen: true
    , type: true
    , typeAnimated: true
    , useBootstrap: false
    , theme: 'material'
    , buttons: {
        ok: {
            text: 'Simpan',
            btnClass: 'btn-primary',
            action: function() {
                var source = productInfoDialog.$content.find('#dialog-content-product-source').val();
                var productId = productInfoDialog.$content.find('#dialog-content-product-id').val();
                var isViolation = this.$content.find('.dialog-content-more-info-violated:checked').val();
                var information = this.$content.find('#dialog-more-info-information').val();
                var officer = this.$content.find('#dialog-more-info-officer').val();
                var deputy = this.$content.find('#dialog-content-more-info-deputy option:selected').val();

                saveMoreInfo(source, productId, isViolation, information, officer, deputy);
                return false;
            }
        },
        close: {
            text: 'Tutup'
        }
    }
    , onContentReady: function() {
        var self = this;
        var data = JSON.parse(productInfoDialog.$content.find('#dialog-content-product-object').val());
        // console.log('onContentReady: '); console.log(JSON.stringify(data));
        
        if (1 === data.is_violating) {
            self.$content.find('#dialog-content-more-info-violated-yes').attr('checked', true);
        } else {
            self.$content.find('#dialog-content-more-info-violated-no').attr('checked', true);
        }

        self.$content.find('#dialog-more-info-information').val(data.information);
        self.$content.find('#dialog-more-info-officer').val(data.officer);
        self.$content.find('#dialog-content-more-info-deputy').val(data.deputy);
    }
});

$('#data-table tbody').on('click', '.data-table-link', function(e) {
    if ('picture' !== $(this).attr('data-selected')) {
        e.preventDefault();
        switch ($(this).attr('data-selected')) {
            case 'product': openProductInfoDialog($(this).attr('data-source'), $(this).attr('data-id')); break;
            case 'store': openStoreInfoDialog($(this).attr('data-source'), $(this).attr('data-id')); break;
            case 'owner': openUserInfoDialog($(this).attr('data-source'), $(this).attr('data-id')); break;
            default: break;
        }
    }
}).on('click', '.btn-violation', function(e) {
    openViolationInfoDialog($(this).attr('data-source'), $(this).attr('data-desc'));
    return false;
});

getSourceList = function() {
    $.loadingBlockShow();

    $.get(API_URL + '/data/Source/get_list', function(response) {
        if ((200 === response.result) && (0 < response.count)) {
            var tempButtons = '';
            $.each(response.data, function(a, b) {
                tempButtons += '<button type="button" class="btn btn-sm btn-default btn-source shadow-header" value="' + b.value + '">';
                tempButtons += b.name + ' (' +  b.count + ')';
                tempButtons += '</button>&nbsp;';
            });

            $('#source-btn-container').html(tempButtons);

            $('.btn-source').click(function(e) {
                $('.btn-source').removeClass('btn-primary');
                $('.btn-source').addClass('btn-default shadow-header');
                $(this).addClass('btn-primary');
                getProductList($(this).val());
            });
        }

        $.loadingBlockHide();
    });
};

getProductList = function(source) {
    $.loadingBlockShow();
    dataTable.rows().remove().draw(false);

    $.get(API_URL + '/data/Product/get_list/' + source, function(response) {
        if ((200 === response.result) && (0 < response.count)) {
            populateProductTable(source, response.data);
        }

        $.loadingBlockHide();
    });
};

populateProductTable = function(source, responseData) {
    var seq = 1;
    var imgLink = '';
    var imgProdTypeUrl = '';
    var imgProdTypeTitle = '';
    var tempExplodedDateTime = [];
    var isViolating = 'Tidak';

    $.each(responseData, function(a, b) {
        imgProdTypeUrl = '';
        imgProdTypeTitle = '';
        if ('' !== b.product_type) {
            imgProdTypeUrl = b.product_type.image;
            imgProdTypeTitle = b.product_type.title;
        }

        imgLink = '<a href="' + HOST_URL + "" + b.image + '" class="data-table-link" target="_blank" data-selected="picture">';
        imgLink += '<img src="' + HOST_URL + "" + b.image + '" style="width:50px; height:50px;">';
        imgLink += '</a>'

        var violationButtons = '';
        if ((null !== b.violation_type) && '' !== (b.violation_type)) {
            $.each(b.violation_type, function(a, b) {
                if (null === b.source) { b.source = ''; }
                if (null === b.description) { b.description = ''; }

                var tempButton = '<button type="button" class="btn btn-sm btn-default btn-violation" value="' + b.label + '" data-source="' + b.source + '" data-desc="' + b.description + '">';
                tempButton += b.label;
                tempButton += '</button>&nbsp;';

                violationButtons += tempButton;
            });
        }

        tempExplodedDateTime = b.date_inserted.split(' ');

        isViolating = 'Tidak';
        if (1 === b.is_violating) {
            isViolating = 'Ya';
        }

        var newRow = dataTable.row.add([
            seq + '.'
            , tempExplodedDateTime[0]
            , b.keyword
            , (0 == b.keyword_deputy) ? '' : b.keyword_deputy
            , '<a href="#" class="data-table-link product-info-link" data-source="' + source + '" data-selected="product" data-id="' + b.product_id + '">' + b.title + '</a>'
            , '<img src="' + imgProdTypeUrl + '" alt="' + imgProdTypeTitle + '" title="' + imgProdTypeTitle + '" style="width:28px; height:28px;">'
            , violationButtons
            , isViolating
            , imgLink
            , '<a href="#" class="data-table-link store-info-link" data-source="' + source + '" data-selected="store" data-id="' + b.store_id + '">' + b.store_name + "</a>"
            , '<a href="#" class="data-table-link user-info-link" data-source="' + source + '" data-selected="owner" data-id="' + b.owner_id + '">' + b.owner_fullname + '</a>'
        ]).draw().node();
        seq++;
    });
};

saveMoreInfo = function(source, productId, isViolation, information, officer, deputy) {
    $.loadingBlockShow();

    // alert(source + ' ' + productId + ' ' + isViolation + ' ' + information + ' ' + officer + ' ' + deputy);
    var data = {
        'source': source,
        'product': productId,
        'violation': isViolation,
        'info': information,
        'officer': officer,
        'deputy': deputy
    };

    var url = API_URL + '/data/Product/save_violation_info';
    $.ajax({
        url : url,
        type : 'POST',
        contentType : 'application/json; charset=utf-8',
        data : JSON.stringify(data),
        dataType : 'json'
    })
    .done(function(json) {
        // console.log(JSON.stringify(json));
        if ((200 === json.result) && (1 === json.count)) {
            var data = JSON.parse(productInfoDialog.$content.find('#dialog-content-product-object').val());
            data.is_violating = isViolation;
            data.information = information;
            data.officer = officer;
            data.deputy = deputy;
            productInfoDialog.$content.find('#dialog-content-product-object').val(JSON.stringify(data));
            moreInfoDialog.close();
        }

        $.loadingBlockHide();
    });
};

openMoreInfoDialog = function() {
    moreInfoDialog.open();
};

openProductInfoDialog = function(source, productid) {
    $.loadingBlockShow();

    productInfoDialog.open();
    productInfoDialog.$content.find('#dialog-content-product-source').val('');
    productInfoDialog.$content.find('#dialog-content-product-id').val('0');
    productInfoDialog.$content.find('#dialog-content-product-object').val('');

    var url = API_URL + '/data/Product/get_data/' + source + '/' + productid;
    $.get(url, function(response) {
        // console.log(JSON.stringify(response));
        if ((200 === response.result) && (0 < response.count)) {
            var data = response.data[0];
            var fullUrl = '<a href="' + data.full_url + '" target="_blank">' + source + '</a>';
            var violationButtons = '';

            if ('' !== data.violation_type) {
                $.each(data.violation_type, function(a, b) {
                    if (null === b.source) { b.source = ''; }
                    if (null === b.description) { b.description = ''; }

                    var tempButton = '<button type="button" class="btn btn-xs btn-default btn-violation" value="' + b.label + '" data-source="' + b.source + '" data-desc="' + b.description + '">';
                    tempButton += b.label;
                    tempButton += '</button>&nbsp;';

                    violationButtons += tempButton;
                });
            }

            productInfoDialog.$content.find('#dialog-content-product-source').val(source);
            productInfoDialog.$content.find('#dialog-content-product-id').val(productid);
            productInfoDialog.$content.find('#dialog-content-product-object').val(JSON.stringify(data));

            productInfoDialog.$content.find('#dialog-content-product-name').text(data.title);
            productInfoDialog.$content.find('#dialog-content-product-store').html(data.store_name);
            productInfoDialog.$content.find('#dialog-content-product-owner').html(data.owner_fullname);
            productInfoDialog.$content.find('#dialog-content-product-source').html(fullUrl);
            productInfoDialog.$content.find('#dialog-content-product-violation').html(violationButtons);
            productInfoDialog.$content.find('#dialog-content-product-description').html(data.full_description);
            productInfoDialog.$content.find('#dialog-content-product-image').attr('src', HOST_URL + "" + data.image);
            productInfoDialog.$content.find('#dialog-content-product-image-link').attr({
                'href': HOST_URL + "" + data.image,
                'alt': data.title,
                'title': data.title
            });

            $('.btn-violation').on('click', function() {
                openViolationInfoDialog($(this).attr('data-source'), $(this).attr('data-desc'));
            });
        }

        $.loadingBlockHide();
    });
};

openStoreInfoDialog = function(source, storeid) {
    $.loadingBlockShow();
    storeInfoDialog.open();

    var url = API_URL + '/data/Store/get_data/' + source + '/' + storeid;
    $.get(url, function(response) {
        // console.log(JSON.stringify(response));
        if ((200 === response.result) && (0 < response.count)) {
            var data = response.data[0];
            var fullUrl = '<a href="' + data.full_url + '" target="_blank">' + data.source_website + '</a>';

            storeInfoDialog.$content.find('#dialog-content-store-name').text(data.store_name);
            storeInfoDialog.$content.find('#dialog-content-store-owner').text(data.owner_fullname);
            storeInfoDialog.$content.find('#dialog-content-store-source').html(fullUrl);
            storeInfoDialog.$content.find('#dialog-content-store-description').html(data.description);
            storeInfoDialog.$content.find('#dialog-content-store-image').attr('src', HOST_URL + "" + data.image[0]);
            storeInfoDialog.$content.find('#dialog-content-store-address').html(data.address);

            if ('' !== data.contact) {
                storeInfoDialog.$content.find('#dialog-content-store-contact').text(decodeURIComponent(data.contact.phone_text));
                storeInfoDialog.$content.find('#dialog-content-store-email').text(decodeURIComponent(data.contact.email_text));
            }
        }

        $.loadingBlockHide();
    });
};

openUserInfoDialog = function(source, userid) {
    $.loadingBlockShow();
    userInfoDialog.open();

    var url = API_URL + '/data/Owner/get_data/' + source + '/' + userid;
    $.get(url, function(response) {
        // console.log(JSON.stringify(response));
        if ((200 === response.result) && (0 < response.count)) {
            var data = response.data[0];
            var fullUrl = '<a href="' + data.full_url + '" target="_blank">' + data.source_website + '</a>';

            userInfoDialog.$content.find('#dialog-content-user-fullname').text(data.fullname);
            userInfoDialog.$content.find('#dialog-content-user-username').text(data.username);
            userInfoDialog.$content.find('#dialog-content-user-location').html(data.address);
            userInfoDialog.$content.find('#dialog-content-user-source').html(fullUrl);
            userInfoDialog.$content.find('#dialog-content-user-description').html(data.description);
            userInfoDialog.$content.find('#dialog-content-user-image').attr('src', HOST_URL + "" + data.image[0]);

            if ('' !== data.contact) {
                userInfoDialog.$content.find('#dialog-content-user-contact').text(decodeURIComponent(data.contact.phone_text));
            }
        }

        $.loadingBlockHide();
    });
};

openViolationInfoDialog = function(source, desc) {
    var sourceUrl = '';
    if ('' !== source) {
        sourceUrl = 'Sumber: <a href="' + source + '" target="_blank">' + source + '</a>';
    }
    violationInfoDialog.open();
    violationInfoDialog.$content.find('#dialog-content-violation-description').text(desc);
    violationInfoDialog.$content.find('#dialog-content-violation-source').html(sourceUrl);
};

$(document).ready(function() {
    $('#dialog-user-info').hide();
    $('#dialog-store-info').hide();
    $('#dialog-product-info').hide();
    $('#dialog-more-info').hide();
    getSourceList();
});

</script>