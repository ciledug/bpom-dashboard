<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accountmodel extends CI_Model {

    function __construct() {
        parent::__construct();		
    }
	
	function get_profile($user_id)
	{
		$returndata = array();
		
		$this->db->select('user_fullname, 
						  user_mail, 
						  user_address, 
						  user_postal, 
						  user_phone, 
						  user_mobile')
					->from('user_profile')
					->where('user_id', $user_id);
		
		$q = $this->db->get();
		
		if ($q && $q->num_rows() > 0)
		{
			$r = $q->row_array(); 
			
			$returndata['name'] 		= $r['user_fullname'];
			$returndata['mail'] 		= $r['user_mail'];
			$returndata['address'] 		= $r['user_address'];
			$returndata['postal'] 		= $r['user_postal'];
			$returndata['phone'] 		= $r['user_phone'];
			$returndata['mobile'] 		= $r['user_mobile'];
			
			return $returndata;
		}
		
		return FALSE;
	}
	
	function update_profile($user_id, $postdata)
	{				
		$updatedata = array(
               'user_fullname'	=> $postdata['name'],
			   'user_address' 	=> $postdata['address'],
               'user_postal' 	=> $postdata['postal'],
               'user_phone' 	=> $postdata['phone'],
			   'user_mobile' 	=> $postdata['mobilephone']
        );

		$this->db->where('user_id', $user_id);
		$q = $this->db->update('user_profile', $updatedata); 
		
		if ($q)
		{
			$sessions = array(
				'name' 		=> $postdata['name'],
				//'mail'		=> $postdata['address'],
				'address'	=> $postdata['address'],
				'postal'	=> $postdata['postal'],
				'phone'		=> $postdata['phone'],
				'mobile'	=> $postdata['mobilephone']
			);
			
			$this->authlib->create_sessions($sessions);
			return TRUE;
		}
			
		return FALSE;
	}
	
	function update_password($user_id, $user_name, $pass)
	{
		$data = array(
        	'user_pass' => $this->authlib->encrypt_pass($user_name, $pass)
        );

		$this->db->where('user_id', $user_id);
		$q = $this->db->update('user_account', $data); 
		
		if ($q) 
			return TRUE;
			
		return FALSE;
	}
	
	function add_new_user($data)
	{
		/*USER ACCOUNT*/
		$this->db->insert('user_account', array(
			'user_name'		=> $data['name'],
			'user_pass'		=> $this->authlib->encrypt_pass($data['name'], $data['pass']),
			'user_group'	=> $data['group'],
			'blocked'		=> $data['blocked']
		));
		
		$user_id = $this->db->insert_id();
		
		if ($user_id && $user_id > 0)
		{
			/*USER PROFILE*/
			$this->db->insert('user_profile', array(
				'user_id'		=> $user_id,
				'user_fullname'	=> $data['fullname'],
				'user_mail'		=> $data['mail'],
				'user_address'	=> $data['address'],
				'user_postal'	=> $data['postal'],
				'user_phone'	=> $data['phone'],
				'user_mobile'	=> $data['mobile']
			));
			
			
			/*USER ADDITIONAL DATA*/
			$this->load->library('gis/gis_util');
			$utm = $this->gis_util->geo_to_utm($data['lon'], $data['lat']);
			
			$this->db->insert('user_additional_data', array(
				'user_id'		=> $user_id,
				'center_lat'	=> $data['lat'],
				'center_lon'	=> $data['lon'],
				'center_x'		=> $utm[0],
				'center_y'		=> $utm[1],
				'use_camera'	=> $data['use_camera']
			));
		}
		
		return $user_id;
	}
	
	function admin_update_user($user_id, $data)
	{
		if (is_numeric($user_id) && $user_id > 0)
		{
			$user_id = (int) $user_id;
			
			/*USER ACCOUNT*/
			$param = array(
				'user_name'		=> $data['name'],
				'user_group'	=> $data['group'],
				'blocked'		=> $data['blocked'],
				'privileges'	=> $data['privileges']
			);
			
			if ($data['pass'] != "")
			{
					$param['user_pass'] = $this->authlib->encrypt_pass($data['name'], $data['pass']);
			}
			
			$this->db->where('user_id', $user_id)
						->update('user_account', $param);
			
			
			/*USER PROFILE*/
			$this->db->where('user_id', $user_id)
						->update('user_profile', array(
								'user_fullname'	=> $data['fullname'],
								'user_mail'		=> $data['mail'],
								'user_address'	=> $data['address'],
								'user_postal'	=> $data['postal'],
								'user_phone'	=> $data['phone'],
								'user_mobile'	=> $data['mobile']
							));
			
			
		}
	}
	
	function delete_user_data($user_id)
	{
		if ((is_array($user_id) && count($user_id) > 0) OR (is_numeric($user_id) && $user_id > 0))
		{
			$table_array = array(
				'user_account',
				'user_brand',
				'user_profile'
			);
			
			foreach ($table_array as $t)
			{
				if (is_array($user_id))
					$this->db->where_in('user_id', $user_id);
				else
					$this->db->where('user_id', (int) $user_id);
				
				$this->db->delete($t);
			} //END FOREACH
		}
	}

	function get_privileges($id) {
		$this->db->select('privileges');
		$r = $this->db->get_where('user_account',array('user_id'=>$id))->row();
		return $r->privileges;
	}

	function show_privileges($group,$root) {
		$where = array(
			'group' => $group,
			'root_id'	=> $root
		);
		return $this->db->get_where('trans_menu',$where);
	}
	
	function getUser($where='',$sort='', $limit='') {
		$sql = "
			SELECT *,DATE_FORMAT(last_login, '%d-%m-%Y %H:%i') AS last_login FROM user_account $where $sort $limit
		";		
		return $this->db->query($sql);
	}
	
	function countUser($where) {
		return $this->getUser($where)->num_rows();  		
	}
	
	function add($param, $id=0) {
		if ($id) {
			$this->db->where('user_id',$id);
			$this->db->update('user_account', $param);
		} else {
		    $command = 'INSERT INTO user_account SET ';
		    $command .= 'user_email=' . $this->db->escape($param['user_email']) . ', ';
		    $command .= 'user_pass=' . $this->db->escape($param['user_pass']) . ', ';
		    $command .= 'user_name=' . $this->db->escape($param['user_email']) . ', ';
		    $command .= 'date_inserted=NOW() ';
		    
		    if ($this->db->simple_query($command)) {
		        $id = $this->db->insert_id();
		    }
		}
		
		return $id;
	}
	
	function saveProfile($param,$id=0) {
		if($id){
			$this->db->where('user_id',$id);
			$this->db->update('user_profile',$param);
		} else
			$this->db->insert('user_profile',$param);
	}
	
	function insert_user_region($id=0,$region) {
		$sql="
			UPDATE user_account
			SET regions = CONCAT(regions,'|','".$region."')
			WHERE user_id = '".$id."'
			";
		$this->db->query($sql);
	}
	
	public function delete_more_user($ids) {	
		$ids=substr($ids,0,-1);	
		$sql = " DELETE FROM user_account WHERE user_id IN (".$ids."); ";		
		$this->db->query($sql);		
	}

}
