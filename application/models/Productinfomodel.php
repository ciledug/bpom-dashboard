<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Productinfomodel.php
 * Created by iwan rahardi p.
 * Created on 01032018 1424
 * Built on Lubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get products data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Productinfomodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getData($source) {
        $data = false;

        $command = 'SELECT p.keyword, p.product_id, p.title, p.product_type, p.violation_type, p.store_id, p.store_name, p.owner_id, p.owner_fullname, p.image, p.is_violating, p.information, p.officer, p.deputy, p.date_inserted ';
        $command .= '       , k.officer AS keyword_officer, k.deputy AS keyword_deputy ';
        $command .= 'FROM products_' . $source . ' p ';
        $command .= 'LEFT JOIN keywords k ON k.keyword = p.keyword ';

        $q = $this->db->query($command);
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
    }

    public function getProductInfo($source, $productId) {
        $data = false;
        $this->db->select('product_id, title, full_url, meta_description, full_description, violation_type, store_name, owner_fullname, image, is_violating, information, officer, deputy, date_inserted');
        $this->db->from('products_' . $source);
        $this->db->where('product_id', $productId);
        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
    }

    public function getCount($source) {
        return $this->db->count_all('products_' . $source);
    }

    public function saveViolationInfo($source, $productId, $data) {
        $processResult = false;
        $this->db->where('product_id', $productId);
        $processResult = $this->db->update('products_' . $source, $data);
        return $processResult;
    }
}