<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Utils {
	
	var $CI;
	
	function __construct() {
		$this->CI =& get_instance();
	}
	
	function createPassword($pass) {
		$this->CI->load->library('encrypt');
		return $this->CI->encrypt->encode($pass);
	}
	
	function decryptPassword($pass) {
		$this->CI->load->library('encrypt');
		return $this->CI->encrypt->decode($pass);
	}

	function createRandomReceipt($userType=null) {
	    $resultRandom = '';
	    $characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	    			   'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	                   '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	    $charLength = count($characters) - 1;
	    
	    for ($i=1; $i<=12; $i++) {
	        $theRandom = rand(0, $charLength);
	        $resultRandom .= $characters[$theRandom];
	    }

	    return substr($resultRandom, 0, strlen($resultRandom) - 1);
	}

	public function createRandomColor() {
		$resultRandom = '';
		$characters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
		$charLength = count($characters) - 1;
	    for ($i=1; $i<=6; $i++) {
	        $theRandom = rand(0, $charLength);
	        $resultRandom .= $characters[$theRandom];
	    }

	    return '#' . $resultRandom;
	}
	
}