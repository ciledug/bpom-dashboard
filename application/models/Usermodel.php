<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model {
	
	function __construct() {
		parent::__construct();
		$this->load->library('encrypt');
	}
	
	function getUserLogin($id)
	{
		$data = false;
		
		if (strpos($id, '@') > -1) {
    		$where = array('user_email' => $id, 'blocked' => 0);
    		$this->db->select('*')->from('user_account')->where($where);
		} else if (is_numeric($id)) {
    		$where = array('user_id' => $id, 'blocked' => 0);
    		$this->db->select('*')->from('user_account')->where($where);
    	} else if (is_array($id)) {
    	    $id['blocked'] = 0;
        	$this->db->select('*')->from('user_account')->where($id);
		}
		
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			$data = $q->row_array();
		}
		
		return $data;
	}

    function getUserFolder($email) {
        $data = false;

        if (strpos($email, '@') > -1) {
            $where = array('user_email' => $email);
            $this->db->select('user_id, user_name')->from('user_account')->where($where);
        }

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }

        return $data;
    }
	
}
