<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Source.php
 * Created by iwan rahardi p.
 * Created on 07032016 0436
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get source website info ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Source extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('sendjson');
        $this->load->model('mainmodel');
        $this->load->model('sourcewebsitemodel');
    }

    public function get_list() {
        $result = 500;
        $count = 0;
        $message = 'FAILED';
        $data = array();

        if ($this->mainmodel->cek_session()) {
            $q = $this->sourcewebsitemodel->getList();

            if ($q) {
                $this->load->model('productinfomodel');

                $row = 0;
                foreach ($q AS $key => $value) {
                    $value->count = $this->productinfomodel->getCount($value->value);
                    $data[] = $value;
                }
                

                $result = 200;
                $count = count($data);
                $message = 'OK';
            }
        }

        $this->sendjson->send(array(
            'result' => $result,
            'count' => $count,
            'message' => $message,
            'data' => $data
        ));
    }
}