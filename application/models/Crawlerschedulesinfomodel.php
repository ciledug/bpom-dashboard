<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Crawlerschedulesinfomodel.php
 * Created by iwan rahardi p.
 * Created on 07032018 0442
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : for general info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Crawlerschedulesinfomodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getList($source) {
        $data = false;
        $this->db->select('name, value');
        $this->db->from('crawler_schedules');
        $this->db->where('website', $source);
        $this->db->order_by('date_finished', 'desc');
        $this->db->limit(1);

        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
    }

    public function getLastRun($source, $keyword) {
        $data = false;

        $command = 'SELECT cs.date_started, cs.date_finished, cs.date_inserted ';
        $command .= 'FROM crawler_schedules cs ';
        $command .= "WHERE cs.website='" . $source . "' ";
        $command .= "AND cs.keyword='" . $keyword . "' ";
        $command .= 'AND cs.status=0 ';
        $command .= 'ORDER BY cs.date_finished DESC ';
        $command .= 'LIMIT 1 ';
        
        $q = $this->db->query($command);
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
    }
}