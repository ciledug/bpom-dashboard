<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once 'include_header.php'; ?>

<div class="container">
    <div class="row first-row">
        <div class="col-md-12 col-sm-12">
            <h3><?php echo $data['title']; ?></h3>
            <hr />
        </div>
    </div>

    <div class="row">
        <div id="source-btn-container" class="col-md-12 col-sm-12">
        </div>
    </div>

    <div class="row"><div class="col-sm-12 col-md-12">&nbsp;</div></div>

    <div class="row">
        <div class="col-sm-5 col-md-5">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <!-- <p>Total pencarian menemukan: <span id="home-total-count-container" class="text-bold text-underline">0</span> produk.</p> -->
                </div>
                
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <table id="data-table-keyword" class="table table-striped table-bordered hover" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Kata Kunci</th>
                                <th>Jumlah</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody id="data-table-body"></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-7 col-md-7">
            <canvas id="home-canvas-summary" width="80%" height="50%"></canvas>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('assets_url'); ?>/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('js_url'); ?>/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('js_url'); ?>/common-vars.js"></script>
<script type="text/javascript">
var ctx = document.getElementById('home-canvas-summary').getContext('2d');
var chartSummary = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: []
        }]
    },
    options: {
        responsive: true
    }
});

var dataTable = $('#data-table-keyword').DataTable({
    ordering: true,
    paging: false,
    info: false,
    searching: false,
    pageLength: 15,
    scrollY: 600,
    columns: [
        { 'width': '70%' }      // keyword
        , { 'width': '10%' }    // jumlah
        , { 'width': '20%' }    // tanggal
    ],
    columnDefs:[
        { targets: [1], orderData: [1, 0], className: 'dt-body-right' }
    ],
    createdRow: function(row, data, index) {
    }
});

getSourceList = function() {
    $.loadingBlockShow();
    $.get(API_URL + '/data/Source/get_list', function(response) {
        if ((200 === response.result) && (0 < response.count)) {
            var tempButtons = '';
            $.each(response.data, function(a, b) {
                tempButtons += '<button type="button" class="btn btn-sm btn-default btn-source shadow-header" value="' + b.value + '">';
                tempButtons += b.name + ' (' + b.count + ')';
                tempButtons += '</button>&nbsp;';
            });

            $('#source-btn-container').html(tempButtons);

            $('.btn-source').click(function(e) {
                $('.btn-source').removeClass('btn-primary');
                $('.btn-source').addClass('btn-default shadow-header');
                $(this).addClass('btn-primary');
                getStatistics($(this).val());
            });
        }

        $.loadingBlockHide();
    });
};

getStatistics = function(source) {
    // console.log(source);

    $.loadingBlockShow();
    dataTable.rows().remove().draw(false);

    $.get(API_URL + '/data/Statistic/get_summary/' + source, function(response) {
        chartSummary.data.labels = [];
        chartSummary.data.datasets = [{
            data: [],
            backgroundColor: []
        }];
        chartSummary.update();

        if ((200 === response.result) && (0 < response.count)) {
            var data = response.data;    
            var bCount = 0;
            var bLast = { date_started: '', date_finished: '', date_inserted: '' };

            $.each(data, function(a, b) {
                bCount = parseInt(b.count, 10);
                bLast = { date_started: '', date_finished: '', date_inserted: '' };
                if (b.last) {
                    bLast = b.last[0];
                }

                if (0 < bCount) {
                    chartSummary.data.labels.push(b.keyword);
                    chartSummary.data.datasets[0].data.push(bCount);
                    chartSummary.data.datasets[0].backgroundColor.push(b.color);
                    chartSummary.update();
                }

                var newRow = dataTable.row.add([
                    b.keyword
                    , bCount
                    , bLast.date_finished.substring(0, bLast.date_finished.indexOf(' '))
                ]).draw().node();
            });
        }

        $.loadingBlockHide();
    });
};

$(document).ready(function() {
    getSourceList();
});
</script>