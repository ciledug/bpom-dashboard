<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendjson {
    
    var $CI;
    
    function __construct() {
        $this->CI =& get_instance();
    }
    
    // function send($data) {
    //     header("Expires: Wed, 01 Jan 2020 00:00:00 GMT" );
    //     header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT");
    //     header("Cache-Control: no-cache, must-revalidate" );
    //     header("Pragma: no-cache" );
    //     header("Content-type: application/json; charset=utf-8");
    //     echo json_encode($data, JSON_NUMERIC_CHECK);
    // }

    function send($data, $isAsText=false) {
        header("Access-Control-Allow-Origin: *");
        header("Expires: Wed, 01 Jan 2020 00:00:00 GMT" );
        header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT");
        header("Cache-Control: no-cache, must-revalidate" );
        header("Pragma: no-cache" );
        header("Content-type: application/json; charset=utf-8");

        if ($isAsText) {
            echo json_encode($data);
        } else {
            echo json_encode($data, JSON_NUMERIC_CHECK);
        }
        
    }
    
    function sendText($text) {
        header("Access-Control-Allow-Origin: *");
        header("Expires: Wed, 01 Jan 2020 00:00:00 GMT" );
        header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT");
        header("Cache-Control: no-cache, must-revalidate" );
        header("Pragma: no-cache" );
        header("Content-type: application/json; charset=utf-8");
        echo $text;
    }
        
}
