<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['folder_name']      = '/bpom-dev';

$config['data_url']         = $config['folder_name'] . '/data';
$config['assets_url']       = $config['folder_name'] . '/assets';
$config['js_url']           = $config['assets_url'] . '/js';
$config['css_url']          = $config['assets_url'] . '/css';
$config['image_url']        = $config['assets_url'] . '/images';

$config['root_directory']   = '/var/www/html' . $config['folder_name'];
$config['data_directory']   = $config['root_directory'] . '/data';

$config['web_title']        = 'BPOM Grabber Administration';
$config['image_no_image']   = $config['image_url'] . '/no_image_available.png';