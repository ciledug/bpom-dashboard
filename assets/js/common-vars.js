/**
 * common-vars.js
 * Created by iwan rahardi p.
 * Created on 28022016 0246
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : JS common vars ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

 var HOST_URL = 'http://127.0.0.1';
 // var HOST_URL = 'http://209.97.169.243';
 var API_URL = HOST_URL + '/bpom-dev';