<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Generalinfomodel.php
 * Created by iwan rahardi p.
 * Created on 01042018 0528
 * Built on Lubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get general info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Generalinfomodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
    }

    public function getKeywords() {
        $data = false;
        $this->db->select('keyword, source_website, store_id, owner_id, violation_type');
        $this->db->from('general_info');
        $q = $this->db->get();
        if ($q) {
            $data = $q->result();
        }
        return $data;
    }
}