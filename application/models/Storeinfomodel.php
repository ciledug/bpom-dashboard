<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Storeinfomodel.php
 * Created by iwan rahardi p.
 * Created on 01032018 1513
 * Built on Ubuntu 16.04
 * Built on Sublime Text 2017
 * Purpose : get store info data ...
 * 
 * Permissions : - you are NOT allowed to COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within
 *                 this file without creator's written permission(s)
 *               - you are NOT allowed, under any circumstances, to COPY, MODIFY, REMOVE, REDISTRIBUTE
 *                 this header comments without creator's written permission(s)
 * 
 * Disclaimer  : this software is provided "as is", so if you have a writen permission from the creator
 *               and you COPY, MODIFY, REMOVE, REDISTRIBUTE part or all content within this file, the
 *               creator CAN NOT be held responsible for any results and/or possible damages and/or
 *               possible collateral damages that MAY or MAY NOT come and/or arise from any of these
 *               process
 */

class Storeinfomodel extends CI_Model {

     public function __construct() {
        parent::__construct();
     }

     public function getStoreInfo($source, $storeId) {
        $data = false;
        $this->db->select('source_website, store_id, store_name, owner_fullname, full_url, description, image');
        $this->db->select('address, latitude, longitude, contact');
        $this->db->from('stores_' . $source);
        $this->db->where('store_id', $storeId);

        $q = $this->db->get();
        if (0 < $q->num_rows()) {
            $data = $q->result();
        }
        return $data;
     }
}