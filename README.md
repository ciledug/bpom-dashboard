# BPOM Crawler Administration Dashboard #

This the old source code for the BPOM Crawler Administration dashboard. The dashboard is used to show
the result from data grabber/scrapper machines and also to schedule the time for the auto grabber/scrapper machines to run.

The full demo can be viewed at [BPOM Crawler Administration](http://103.5.148.213/bpom) demo page.

* PHP (CodeIgniter 3)
* Bootstrap 4
* MySQL
* jQuery
* AJAX
* JSON
